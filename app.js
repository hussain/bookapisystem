var restify = require("restify");
var server = restify.createServer();
var pug = require("pug");
var fs = require("fs");
var sessions = require("client-sessions");

server.use(sessions({
  cookieName: "session",
  secret: "SECRET",
  duration: 24 * 60 * 60 * 1000,
  activeDuration: 5 * 60 * 1000
}));

server.use(function(req, res, next) {
  res.render = function(file, options) {
    if(!options) options = {};
    options.customer = req.session.customer;
    options.admin = req.session.admin;
    if (file.indexOf(".pug") != -1) {
      var html = pug.renderFile("./" + file, options);
      res.writeHead(200, { "Context-Type": "text/html" });
      res.write(html);
      res.end();
    } else if (file.indexOf(".css") != -1) {
      fs.readFile("./" + file, function(error, data) {
        res.writeHead(200, { "Context-Type": "text/css" });
        res.write(data);
        res.end();
      });
    } else if (file.indexOf(".js") != -1) {
      fs.readFile("./" + file, function(error, data) {
        res.writeHead(200, { "Context-Type": "text/javascript" });
        res.write(data);
        res.end();
      });
    }
  };
  return next();
});

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.IP = process.env.IP || "127.0.0.1";
server.port = process.env.PORT || 3000;
var mongojs = require("mongojs");
var db = mongojs("mongodb://" + server.IP + "/bookapisystem", ["books", "customers", "admins"]);
var google = require("googleapis");
var books = google.books({ locale: "en_US", version: "v1", auth: "AIzaSyA3wQsI4uapZlLEYNvvUZ4O8Hrs1tzhAeU" });

server.post("/api/books", function(req, res, next) {
  books.volumes.list({ q: "isbn+" + req.params.isbn }, function(error, result) {
    console.log(books);
    var book = {
      isbn: req.params.isbn,
      price: req.params.price,
      title: result.items[0].volumeInfo.title,
      author: result.items[0].volumeInfo.authors[0]
    };
    db.books.save(book,
    function(error, book) {
      res.send(book);
    });
  });
  return next();
});

server.put("/api/books/:isbn", function(req, res, next) {
  db.books.findOne({ isbn: req.params.isbn }, function(error, book) {
    book.stock = req.params.stock;
    db.books.update({ isbn: req.params.isbn }, book, { multi: false }, function(error, data) {
      res.send(book);
    });
  });
  return next();
});

server.del("/api/books/:isbn", function(req, res, next) {
  db.books.remove({ isbn: req.params.isbn }, function(error, book) {
    res.send(book);
  });
  return next();
});

server.get("/api/books", function(req, res, next) {
  db.books.find(function(error, books) {
    res.send(books);
  });
  return next();
});

server.get("/api/books/:isbn", function(req, res, next) {
  db.books.findOne({ isbn: req.params.isbn }, function(error, book) {
    res.send(book);
  });
  return next();
});

server.get("/public/css/style.css", function(req, res, next) {
  res.render("public/css/style.css");
});

server.get("/public/js/script.js", function(req, res, next) {
  res.render("public/js/script.js");
});

// customer

server.get("/", function(req, res, next) {
  db.books.find({}, function(error, books) {
    res.render("views/index.pug", { books: books });
  });
});

server.get("/admin/books/new", function(req, res, next) {
  res.render("views/admin/new.pug");
});

server.get("/books/:isbn", function(req, res, next) {
  db.books.findOne({ isbn: req.params.isbn }, function(error, book) {
    res.render("views/show.pug", { book: book });
  });
});

server.get("/login", function(req, res, next) {
  res.render("views/login.pug");
});

server.post("/login", function(req, res, next) {
  db.customers.findOne({ email: req.params.email, password: req.params.password }, function(error, customer) {
    if (customer == null) {
      res.render("views/login.pug", { error: "Invalid email/password." });
    } else {
      req.session.customer = customer;
      res.redirect("/", next);
    }
  });
  return next();
});

server.get("/logout", function(req, res, next) {
  req.session.customer = null;
  res.redirect("/", next);
});

server.get("/signup", function(req, res, next) {
  res.render("views/signup.pug");
});

server.post("/signup", function(req, res, next) {
  db.customers.insert({ name: req.params.name, email: req.params.email, password: req.params.password }, function(error, customer) {
    req.session.customer = customer;
    res.redirect("/", next);
  });
});

// admin

server.get("/admin", function(req, res, next) {
  db.books.find({}, function(error, books) {
    res.render("views/admin/index.pug", { books: books });
  });
});

server.get("/admin/login", function(req, res, next) {
  res.render("views/admin/login.pug");
});

server.post("/admin/login", function(req, res, next) {
  db.admins.findOne({ email: req.params.email, password: req.params.password }, function(error, admin) {
    if (admin == null) {
      res.render("views/admin/login.pug", { error: "Invalid email/password." });
    } else {
      req.session.admin = admin;
      res.redirect("/admin", next);
    }
  });
  return next();
});

server.get("/admin/logout", function(req, res, next) {
  req.session.admin = null;
  res.redirect("/admin", next);
});

server.get("/admin/signup", function(req, res, next) {
  res.render("views/admin/signup.pug");
});

server.post("/admin/signup", function(req, res, next) {
  db.admins.insert({ name: req.params.name, email: req.params.email, password: req.params.password }, function(error, admin) {
    req.session.admin = admin;
    res.redirect("/admin", next);
  });
});

server.get("/admin/books/:isbn", function(req, res, next) {
  db.books.findOne({ isbn: req.params.isbn }, function(error, book) {
    res.render("views/show.pug", { book: book });
  });
});

server.post("/admin/books", function(req, res, next) {
  books.volumes.list({ q: "isbn+" + req.params.isbn }, function(error, result) {
    console.log(JSON.stringify(result));
    var book = {
      isbn: req.params.isbn,
      price: req.params.price,
      stock: req.params.stock,
      title: result.items[0].volumeInfo.title,
      author: result.items[0].volumeInfo.authors[0]
    };
    db.books.save(book,
    function(error, book) {
      if (error) res.render("views/admin/books/new", { error: error.message });
      res.redirect("/admin", next);
    });
  });
  return next();
});


server.get("/admin/books/:isbn/edit", function(req, res, next) {
      db.books.findOne({ isbn: req.params.isbn }, function(error, book) {
        res.render("views/admin/edit.pug", { book: book });
      });
});

server.post("/admin/books/:isbn", function(req, res, next) {
  db.books.findOne({ isbn: req.params.isbn }, function(error, book) {
    book.stock = req.params.stock;
    db.books.update({ isbn: req.params.isbn }, book, { multi: false }, function(error, data) {
      if (error) res.render("/admin/books/:isbn/adjustStock", { error: error.message });
      res.redirect("/admin", next);
    });
  });
  return next();
});

server.post("/admin/books/:isbn/delete", function(req, res, next) {
  db.books.remove({ isbn: req.params.isbn }, function(error, book) {
    res.redirect("/admin", next);
  });
  return next();
});

server.post("/books/:isbn", function(req, res, next) {
  db.books.findOne({ isbn: req.params.isbn }, function(error, book) {
    if (!req.session.books) req.session.books = {};
    if(!req.session.books[req.params.isbn]) req.session.books[req.params.isbn] = book;
    if(!req.session.books[req.params.isbn].quantity) req.session.books[req.params.isbn].quantity = 0;
    if(req.session.books[req.params.isbn].quantity < book.stock) req.session.books[req.params.isbn].quantity++;
    if(req.session.books[req.params.isbn].quantity == 0) delete req.session.books[req.params.isbn];
    res.redirect("/", next);
  });
});

server.post("/books/:isbn/remove", function(req, res, next) {
  db.books.findOne({ isbn: req.params.isbn }, function(error, book) {
    if(req.session.books[req.params.isbn].quantity > 1) req.session.books[req.params.isbn].quantity--;
    else delete req.session.books[req.params.isbn];
    res.redirect("/cart", next);
  });
});

server.get("/cart", function(req, res, next) {
  res.render("views/cart.pug", { books: req.session.books });
});

server.post("/books/:isbn/checkout", function(req, res, next) {
  for (var book in req.session.books) {
    var theBook = req.session.books[book];
    theBook.stock = (parseInt(theBook.stock) - theBook.quantity).toString();
    delete theBook._id;
    delete theBook.quantity;
    db.books.update({ isbn: theBook.isbn }, theBook, { multi: false }, function(error, data) {
      console.log(error);
      if (error) res.render("/cart", { error: error.message });
    });
  }
  delete req.session.books;
  res.redirect("/", next);
});


server.listen(server.port, server.IP, function() {
  console.log("bookapisystem running at http://%s:%s", server.IP, server.port);
});
